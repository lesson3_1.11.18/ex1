#include "Vector.h"
#include <iostream>

Vector::Vector(int n) :_capacity((n <= 2) ? 2 : n), _resizeFactor(n), _elements(new int[n]) {}

Vector::~Vector()
{
	delete[] _elements;
	this->_elements = nullptr;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	return this->_size == 0;
}

void Vector::push_back(const int & val)
{
	int* newElements = nullptr;
	int i = 0;

	//if the vector is full so to resize it
	if (this->_size >= this->_capacity)
	{
		this->_capacity = (this->_size + this->_resizeFactor);//resizing
		newElements = new int[this->_capacity];//creating new elements array
		for (i = 0; i < this->_size; i++)
		{
			newElements[i] = this->_elements[i];//copying the elements
		}
		delete[] this->_elements;//deleting previuse elements array
		this->_elements = newElements;
	}
	//Adding the elememt to the end
	this->_elements[this->_size] = val;
	this->_size++;
}

int Vector::pop_back()
{
	int res = (this->_size == 0 ) ? -9999 : (this->_elements[this->_size - 1]);
	if (res != -9999)
		this->_size--;
	else
		std::cout << "error: pop from empty vector" << std::endl;
	return res;
}

void Vector::reserve(int n)
{
	int* newElements = nullptr;
	int i = 0;
	int newSize = (n / this->_resizeFactor) + ((n % this->_resizeFactor != 0) ? 1 : 0);// the amount i need to add(resizeFactor * newSize), the addition is if the reziseFactor is not a multiplation of the n

	
	this->_capacity = (this->_resizeFactor * newSize);
	newElements = new int[this->_capacity];
	for (i = 0; i < this->_size; i++)
	{
		newElements[i] = this->_elements[i];
	}
	delete[] this->_elements;
	this->_elements = newElements;
}

void Vector::resize(int n)
{
	int* newElements = nullptr;
	int i = 0;

	if (n > this->_capacity)
	{
		this->reserve(n);
	}
	else
	{
		this->reserve(n);
		this->_capacity = n;
	}
	/*
	else if (n < this->_capacity)
	{
		this->_capacity = n;
		for (i = 0; i < ((this->_capacity < this->_size) ? this->_capacity : this->_size); i++)
		{
			newElements[i] = this->_elements[i];
		}
		delete[] this->_elements;
		this->_elements = newElements;
	}*/
}

void Vector::assign(int val)
{
	int i = 0;
	for (i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}

void Vector::resize(int n, const int& val)
{
	int i = 0;

	this->resize(n);
	this->assign(val);
}

Vector::Vector(const Vector& other)
{
	*this = other;
}

Vector& Vector::operator=(const Vector& other)
{
	int i = 0;
	//overloading the = operator
	this->_capacity = other.capacity();
	this->_size = other.size();
	this->_resizeFactor = other.resizeFactor();
	delete[] this->_elements;
	this->_elements = new int[this->_capacity];

	for (i = 0; i < this->_size; i++)
	{
		this->_elements[i] = other[i];
	}

	return *this;
}

int& Vector::operator[](int n) const
{
	if (n >= this->_size)
		std::cout << "error: out of bounds" << std::endl;
	return (n < this->_size) ? this->_elements[n] : this->_elements[0];
}

Vector & Vector::operator+(Vector & v)
{
	Vector* temp = new Vector(v.capacity());
	int i = 0;

	for (i = 0; i < this->_size; i++)
	{
		temp->push_back(this->_elements[i] + v[i]);
	}

	return *temp;
}

Vector& Vector::operator+=(Vector & v)
{
	return *this + v;
}

Vector & Vector::operator-(Vector & v)
{
	Vector* temp = new Vector(v.capacity());
	int i = 0;

	for (i = 0; i < this->_size; i++)
	{
		temp->push_back(this->_elements[i] - v[i]);
	}

	return *temp;
}

Vector& Vector::operator-=(Vector & v)
{
	return *this - v;
}

std::ostream & operator<<(std::ostream & os, Vector & v1)
{
	int i = 0;
	std::string elems = "";//all the elements string
	std::stringstream ss;// a stringstream object to convert the int elements to string
	for (i = 0; i < v1.size(); i++)
	{
		ss.str("");//clearing the stringstream object
		ss << v1[i];//adding the current element
		elems += ss.str();//adding the string elements to the elements string
		if(v1.size() - i > 1)//if the element is not the last to put a comma between, otherwise dont
			elems += ",";
	}
	//creating the complete string to return
	os << "Vector info:" << std::endl
		<< "Capacity is " << v1.capacity() << std::endl
		<< "Size is " << v1.size() << std::endl
		<< "data is {" << elems << "}" << std::endl;
	
	return os;
}
