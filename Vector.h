#pragma once

#include <sstream>
#include <iostream>
#include <string>

class Vector
{
private:
	//Fields
	int* _elements;
	int _capacity; //Total memory allocated
	int _size; //Size of vector to access
	int _resizeFactor; // how many cells to add when need to reallocate
public:

	//A
	Vector(int n);
	~Vector();
	int size() const;//return size of vector
	int capacity() const;//return capacity of vector
	int resizeFactor() const; //return vector's resizeFactor
	bool empty() const; //returns true if size = 0

						//B
						//Modifiers
	void push_back(const int& val);//adds element at the end of the vector
	int pop_back();//removes and returns the last element of the vector
	void reserve(int n);//change the capacity
	void resize(int n);//change _size to n, unless n is greater than the vector's capacity
	void assign(int val);//assigns val to all elemnts
	void resize(int n, const int& val);//same as above, if new elements added their value is val

								//C
								//The big three (d'tor is above)
	Vector(const Vector& other);
	Vector& operator=(const Vector& other);

	//D
	//Element Access
	int& operator[](int n) const;//n'th element
	
	//Bonusses
	friend std::ostream& operator<<(std::ostream& os, Vector& v1);// To overload the << operator, returns a string in format of:
																	//Vector Info:
																	//Capacity is {capacity}
																	//Size is {size}
																	//data is {val1, val2, val3,�,valn}
	
	Vector& operator+(Vector& v);// operator +, adds each elements to the other one form the same index, and return new vector
								// v1 = {1,2,3,4}, v2 = {10,20,30,40}
								// returns {11,22,33,44}

	Vector& operator+=(Vector& v);// operator +=, adds each elements to the other one form the same index and save at the left-hand of the +=
								// v1 = {1,2,3,4}, v2 = {10,20,30,40}
								//  v1 = {11,22,33,44}

	Vector& operator-(Vector& v);// operator -, adds each elements to the other one form the same index, and return new vector
								 // v1 = {1,2,3,4}, v2 = {10,20,30,40}
								 // returns {-9,-18,-27,-36}

	Vector& operator-=(Vector& v);// operator -=, adds each elements to the other one form the same index and save at the left-hand of the +=
								  // v1 = {1,2,3,4}, v2 = {10,20,30,40}
								  //  v1 = {-9,-18,-27,-36}
};